package com.example.counterapp

import com.example.statemachine.StateMachine
import com.example.statemachinetest.MainCoroutineRule
import com.example.statemachinetest.stateMachineTest
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class CounterStateMachineTest {

    @get:Rule
    val coroutineTestRule = MainCoroutineRule()

    private val stateMachine: (CoroutineScope) -> StateMachine<CounterState, CounterEvent> =
        {coroutineScope -> CounterStateMachine(coroutineScope) }

    @Test
    fun turnsToErrorState() {
        stateMachineTest(
            stateMachine = stateMachine,
            events = listOf(
                CounterEvent.Plus,
                CounterEvent.Plus,
                CounterEvent.Plus,
                CounterEvent.Minus,
                CounterEvent.Plus,
                CounterEvent.Minus
            ),
            expectedStates = listOf(
                CounterState(0),
                CounterState(1),
                CounterState(2),
                CounterState(3),
                CounterState(2),
                CounterState(3),
                CounterState(2),
            ),
            coroutineScope = coroutineTestRule,
        )
    }

}
