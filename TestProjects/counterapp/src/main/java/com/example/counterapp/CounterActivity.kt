package com.example.counterapp

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.counterapp.databinding.ActivityCounterBinding

import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class CounterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCounterBinding

    private val viewModel by viewModels<CounterViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCounterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        observeViewModelStates()
        setUpButtons()
    }

    private fun observeViewModelStates() {
        lifecycleScope.launch {
            viewModel.counterStateMachine.states.collect { counterState ->
                binding.text.text = counterState.counter.toString()
            }
        }
    }

    private fun setUpButtons() {
        binding.minus.setOnClickListener {
            viewModel.counterStateMachine.emitEvent(CounterEvent.Minus)
        }

        binding.plus.setOnClickListener {
            viewModel.counterStateMachine.emitEvent(CounterEvent.Plus)
        }
    }
}
