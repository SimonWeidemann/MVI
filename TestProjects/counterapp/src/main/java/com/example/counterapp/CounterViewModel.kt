package com.example.counterapp

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.plus

class CounterViewModel : ViewModel() {

    val counterStateMachine = CounterStateMachine(scope = viewModelScope + Dispatchers.Default)
}
