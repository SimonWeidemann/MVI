package com.example.counterapp

import com.example.statemachine.MulticastingStateMachineCallbacks
import com.example.statemachine.StateMachineCallback
import java.util.logging.Logger

class LoggerCounterStateMachineCallback : StateMachineCallback<CounterState, CounterEvent> {

    private val logger = Logger.getLogger(javaClass.name)

    override fun eventReceived(event: CounterEvent, currentState: CounterState) {
        logger.info("Event $event, currentState: $currentState")
    }

    override fun eventMapped(event: CounterEvent, currentState: CounterState) {
        logger.info("Event $event, currentState: $currentState")
    }

    override fun afterStateEmitted(previousState: CounterState, event: CounterEvent, newState: CounterState) {
        logger.info("previousState: $previousState, event: $event, newState: $newState")
    }
}

class ConsoleCounterStateMachineCallback : StateMachineCallback<CounterState, CounterEvent> {

    private val tag = javaClass.name

    override fun eventReceived(event: CounterEvent, currentState: CounterState) {
        println("$tag, Event $event, Current State: $currentState")
    }

    override fun eventMapped(event: CounterEvent, currentState: CounterState) {
        println("$tag, Event $event, Current State: $currentState")
    }

    override fun afterStateEmitted(previousState: CounterState, event: CounterEvent, newState: CounterState) {
        println("$tag, Previous State: $previousState, Event: $event, New State: $newState")
    }
}

object MulticastingStateMachineCallback : MulticastingStateMachineCallbacks<CounterState, CounterEvent> {
    override val callbacks: List<StateMachineCallback<CounterState, CounterEvent>> = listOf(
        LoggerCounterStateMachineCallback(),
        ConsoleCounterStateMachineCallback()
    )
}
