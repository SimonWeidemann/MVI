package com.example.counterapp

import com.example.statemachine.StateMachine
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class CounterStateMachine(
    scope: CoroutineScope
) : StateMachine<CounterState, CounterEvent>(
    initialState = CounterState(0),
    stateMachineCallback = MulticastingStateMachineCallback,
    coroutineScope = scope
) {
    override fun mapEventToState(event: CounterEvent): Flow<CounterState> =
        when (event) {
            CounterEvent.Plus -> mapPlus()
            CounterEvent.Minus -> mapMinus()
        }

    private fun mapPlus() = flow {
        val state = currentState
        emit(state.copy(counter = state.counter + 1))
    }

    private fun mapMinus() = flow {
        val state = currentState
        emit(state.copy(counter = state.counter - 1))
    }
}

data class CounterState(val counter: Int)

sealed class CounterEvent {
    object Plus : CounterEvent()
    object Minus : CounterEvent()
}
