package com.example.loadapp

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.loadapp.databinding.ActivityLoadBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class LoadActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoadBinding

    private val viewModel by viewModels<LoadViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoadBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setUpButtons()
        observeViewModelState()
    }

    private fun setUpButtons() {
        binding.Load.setOnClickListener {
            viewModel.loadingStateMachine.emitEvent(LoadEvent.Load)
        }
        binding.Error.setOnClickListener {
            viewModel.loadingStateMachine.emitEvent(LoadEvent.Error)
        }
    }

    private fun observeViewModelState() {
        lifecycleScope.launch {
            viewModel.loadingStateMachine.states.collect { newState ->
                binding.text.text = when (newState) {
                    is LoadState.Default -> "Ready To Load"
                    is LoadState.Finish -> if (newState.error.isEmpty()) "Loading Success" else newState.error
                    is LoadState.Loading -> "..."
                }
            }
        }
    }
}
