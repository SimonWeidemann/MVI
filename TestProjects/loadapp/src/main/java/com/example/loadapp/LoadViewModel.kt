package com.example.loadapp

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.plus

class LoadViewModel : ViewModel() {
    val loadingStateMachine: LoadingStateMachine =
        LoadingStateMachine(
            coroutineScope = viewModelScope + Dispatchers.Default
        )
}




