package com.example.loadapp

import com.example.statemachine.StateMachine
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

class LoadingStateMachine(
    private val coroutineScope: CoroutineScope,
) : StateMachine<LoadState, LoadEvent>(
    initialState = LoadState.Default,
    coroutineScope = coroutineScope,
) {

    override fun mapEventToState(event: LoadEvent): Flow<LoadState> = flow {
        when (event) {
            LoadEvent.Error -> this.mapError()
            LoadEvent.Load -> this.mapLoad()
        }
    }

    private suspend fun FlowCollector<LoadState>.mapError() {
        emit(LoadState.Finish("Error"))
    }

    private suspend fun FlowCollector<LoadState>.mapLoad() {
        when (currentState) {
            is LoadState.Default,
            is LoadState.Finish -> loadSomethingLong()
            is LoadState.Loading -> waitForState()
        }
    }

    private suspend fun FlowCollector<LoadState>.loadSomethingLong() {
        emit(LoadState.Loading)
        delay(3000)
        emit(LoadState.Default)
    }

    private suspend fun FlowCollector<LoadState>.waitForState() {
        states.collect { loadState ->
            when (loadState) {
                is LoadState.Default,
                is LoadState.Finish -> {
                    loadSomethingLong()
                    currentCoroutineContext().cancel()
                }
                else -> Unit
            }
        }
    }

    override suspend fun collectTransition(events: ReceiveChannel<LoadEvent>, handleEventCallback: suspend (LoadEvent) -> Unit) {
        events.consumeEach { event ->
            coroutineScope.launch {
                handleEventCallback(event)
            }
        }
    }
}

sealed class LoadState {
    object Default : LoadState()
    object Loading : LoadState()
    data class Finish(val error: String = "") : LoadState()
}

sealed class LoadEvent {
    object Load : LoadEvent()
    object Error : LoadEvent()
}

