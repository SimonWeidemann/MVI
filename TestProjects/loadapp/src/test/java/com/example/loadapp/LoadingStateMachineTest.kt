package com.example.loadapp

import com.example.statemachine.StateMachine
import com.example.statemachinetest.MainCoroutineRule
import com.example.statemachinetest.stateMachineTest
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class LoadingStateMachineTest {

    @get:Rule
    val coroutineTestRule = MainCoroutineRule()

    private val stateMachine: (CoroutineScope) -> StateMachine<LoadState, LoadEvent> =
        { coroutineScope -> LoadingStateMachine(coroutineScope) }

    @Test
    fun turnsToErrorState() {
        stateMachineTest(
            stateMachine = stateMachine,
            events = listOf(LoadEvent.Load, LoadEvent.Error),
            expectedStates = listOf(LoadState.Default, LoadState.Loading, LoadState.Finish("Error")),
            coroutineScope= coroutineTestRule
        )
    }

    @Test
    fun turnToDefaultState() {
        stateMachineTest(
            stateMachine = stateMachine,
            events = listOf(LoadEvent.Load, LoadEvent.Load),
            expectedStates = listOf(LoadState.Default, LoadState.Loading, LoadState.Default, LoadState.Loading, LoadState.Default),
            coroutineScope= coroutineTestRule
        )
    }
}
