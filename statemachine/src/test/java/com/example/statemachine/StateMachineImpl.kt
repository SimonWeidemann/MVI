package com.example.statemachine

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class StateMachineImpl(
    private val coroutineScope: CoroutineScope
) : StateMachine<State, Event>(
    initialState = State.Default,
    coroutineScope = coroutineScope,
) {

    override fun mapEventToState(event: Event): Flow<State> = flow {
        when (event) {
            is Event.Load -> mapLoad()
            is Event.Finish -> emit(value = State.Final)
        }
    }

    private suspend fun FlowCollector<State>.mapLoad() {
        when (currentState) {
            is State.Default,
            is State.Final -> loadSomethingLong()
            is State.Loading -> waitForState()
        }
    }

    private suspend fun FlowCollector<State>.waitForState() {
        states.collect { state ->
            when (state) {
                is State.Default,
                is State.Final -> {
                    loadSomethingLong()
                    currentCoroutineContext().cancel()
                }
                is State.Loading -> Unit
            }
        }
    }

    private suspend fun FlowCollector<State>.loadSomethingLong() {
        emit(State.Loading)
        delay(3000)
        if (currentState is State.Loading) {
            emit(State.Default)
        }
    }

    override suspend fun collectTransition(events: ReceiveChannel<Event>, handleEventCallback: suspend (event: Event) -> Unit) {
        events.consumeEach { event ->
            coroutineScope.launch {
                handleEventCallback(event)
            }
        }
    }
}

sealed class State {
    object Default : State()
    object Loading : State()
    object Final : State()
}

sealed class Event {
    object Load : Event()
    object Finish : Event()
}
