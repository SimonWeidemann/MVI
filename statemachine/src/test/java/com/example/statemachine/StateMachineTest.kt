package com.example.statemachine

import com.example.statemachinetest.MainCoroutineRule
import com.example.statemachinetest.stateMachineTest
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.plus
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class StateMachineTest {

    @get:Rule
    val coroutineTestRule = MainCoroutineRule()

    private val stateMachine: (CoroutineScope) -> StateMachine<State, Event> =
        {coroutineScope -> StateMachineImpl(coroutineScope + Dispatchers.Main) }

    @Test
    fun `goes to Final state`() {
        stateMachineTest(
            stateMachine = stateMachine,
            events = listOf(Event.Load, Event.Finish),
            expectedStates = listOf(State.Default, State.Loading, State.Final),
            coroutineScope = coroutineTestRule
        )
    }

    @Test
    fun `normal workflow`(){
        stateMachineTest(
            stateMachine = stateMachine,
            events = listOf(Event.Load, Event.Load),
            expectedStates = listOf(
                State.Default,
                State.Loading,
                State.Default,
                State.Loading,
                State.Default),
            coroutineScope = coroutineTestRule
        )
    }
}
