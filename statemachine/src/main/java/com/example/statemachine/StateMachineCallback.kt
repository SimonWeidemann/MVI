package com.example.statemachine

interface StateMachineCallback<State : Any, Event : Any> {

    fun eventReceived(event: Event, currentState: State)
    fun eventMapped(event: Event, currentState: State)
    fun afterStateEmitted(previousState: State, event: Event, newState: State)
}

interface MulticastingStateMachineCallbacks<State : Any, Event : Any> : StateMachineCallback<State, Event> {

    val callbacks: List<StateMachineCallback<State, Event>>

    override fun eventReceived(event: Event, currentState: State) {
        callbacks.forEach { stateMachineCallback ->
            stateMachineCallback.eventReceived(event, currentState)
        }
    }

    override fun afterStateEmitted(previousState: State, event: Event, newState: State) {
        callbacks.forEach { stateMachineCallback ->
            stateMachineCallback.afterStateEmitted(previousState, event, newState)
        }
    }

    override fun eventMapped(event: Event, currentState: State) {
        callbacks.forEach { stateMachineCallback ->
            stateMachineCallback.eventMapped(event, currentState)
        }
    }
}

open class SimpleStateMachineCallbackImpl<State : Any, Event : Any> : StateMachineCallback<State, Event> {

    override fun eventReceived(event: Event, currentState: State) = Unit

    override fun eventMapped(event: Event, currentState: State) = Unit

    override fun afterStateEmitted(previousState: State, event: Event, newState: State) = Unit
}
