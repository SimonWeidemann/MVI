package com.example.statemachine

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

abstract class StateMachine<State : Any, Event : Any>(
    initialState: State,
    coroutineScope: CoroutineScope,
    private val stateMachineCallback: StateMachineCallback<State, Event> = SimpleStateMachineCallbackImpl(),
    private val _state: MutableStateFlow<State> = MutableStateFlow(value = initialState),
    private val events: Channel<Event> = Channel(capacity = Channel.UNLIMITED),
) {

    val states: StateFlow<State>
        get() = _state

    val currentState
        get() = states.value

    private val scope: CoroutineScope = coroutineScope

    init {
        consumeEvents()
    }

    private fun consumeEvents() {
        scope.launch {
            collectTransition(events = events, handleEventCallback = ::mapEventToStateCallback)
        }
    }

    /**
     * This Method should be Open to Override to modify the behaviour,
     * how the Events are consumed and States are emitted.
     */

    open suspend fun collectTransition(
        events: ReceiveChannel<Event>,
        handleEventCallback: suspend (Event) -> Unit
    ) {
        events.consumeEach { event ->
            handleEventCallback(event)
        }
    }

    private suspend fun mapEventToStateCallback(event: Event) {
        stateMachineCallback.eventMapped(event = event, currentState = currentState)
        mapEventToState(event = event).collect { newState ->
            emitState(newState, event)
        }
    }

    abstract fun mapEventToState(event: Event): Flow<State>

    private suspend fun emitState(newState: State, event: Event) {
        val previousState = currentState
        _state.emit(value = newState)
        stateMachineCallback.afterStateEmitted(previousState = previousState, event = event, newState = newState)
    }

    fun emitEvent(newEvent: Event) {
        scope.launch {
            events.send(element = newEvent)
            stateMachineCallback.eventReceived(event = newEvent, currentState)
        }
    }
}
