package com.example.statemachinetest

import com.example.statemachine.StateMachine
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.withTimeout
import org.junit.Assert.assertEquals
import kotlin.time.Duration

@ExperimentalCoroutinesApi
fun <Event : Any, State : Any> stateMachineTest(
    stateMachine: (CoroutineScope) -> StateMachine<State, Event>,
    events: List<Event>,
    expectedStates: List<State>,
    coroutineScope: CoroutineScope,
    timeoutBetweenCollection: Long = 5_000
) {
    runBlockingTest(coroutineScope.coroutineContext) {
        coroutineScope {
            val machine = stateMachine(this)
            val channel = Channel<State>(capacity = Channel.UNLIMITED)
            launch {
                machine.states.take(expectedStates.size).collect { newState ->
                    channel.send(newState)
                }
            }
            events.forEach(machine::emitEvent)
            expectedStates.forEach { expectedState ->
                withTimeout(timeoutBetweenCollection) {
                    assertEquals(channel.receive(), expectedState)
                }
            }
            this.coroutineContext.cancelChildren()
        }
    }
}

